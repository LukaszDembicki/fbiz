<?php
declare(strict_types=1);

namespace App\Shared\Filesystem;

interface FilesystemInterface
{
    public function getPublicFolderPath(): string;
}
<?php
declare(strict_types=1);

namespace App\Shared\Filesystem;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Filesystem implements FilesystemInterface
{
    public function __construct(
        private ParameterBagInterface $parameterBag
    )
    {
    }

    public function getPublicFolderPath(): string
    {
        return $this->parameterBag->get('kernel.project_dir') . '/public';
    }
}
<?php
declare(strict_types=1);

namespace App\Product\Presentation\Api;

use App\Product\Application\Command\AddProduct\AddProduct;
use App\Product\Application\Command\RemoveProduct\RemoveProduct;
use App\Product\Application\Command\UpdateProduct\UpdateProduct;
use App\Product\Application\DTO\ProductDTO;
use App\Product\Application\Query\ProductList\ProductList;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

class ProductController extends AbstractController
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private SerializerInterface $serializer
    )
    {
    }

    public function list(Request $request): Response
    {
        try {
            $productList = $this->messageBus->dispatch(new ProductList(
                    $request->get('page') ? (int)$request->get('page') : 1,
                    $request->get('limit') ? (int)$request->get('limit') : 3
                )
            );

            return new JsonResponse(
                $this->serializer->serialize($productList->last(HandledStamp::class)->getResult()->toArray(), 'json'),
                Response::HTTP_OK,
                [],
                true
            );
        } catch (\Exception $exception) {
            return new JsonResponse(['messsage' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function add(Request $request): Response
    {
        try {
            $this->messageBus->dispatch(new AddProduct($this->serializer->deserialize($request->getContent(), ProductDTO::class, 'json')));

            return new JsonResponse('', Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return new JsonResponse(['messsage' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function patch(Request $request): Response
    {
        try {
            $this->messageBus->dispatch(new UpdateProduct($this->serializer->deserialize($request->getContent(), ProductDTO::class, 'json')));

            return new JsonResponse('', Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return new JsonResponse(['messsage' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function remove(Request $request, int $productId): Response
    {
        try {
            $this->messageBus->dispatch(new RemoveProduct($productId));

            return new JsonResponse('', Response::HTTP_OK);
        } catch (\Exception $exception) {
            return new JsonResponse(['messsage' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
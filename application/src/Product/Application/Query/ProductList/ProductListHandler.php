<?php
declare(strict_types=1);

namespace App\Product\Application\Query\ProductList;

use App\Product\Application\DTO\ProductDTO;
use App\Product\Domain\Persistence\ProductRepositoryInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductListHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository
    )
    {
    }

    /**
     * @param ProductList $productList
     * @return ArrayCollection<ProductDTO>
     */
    public function __invoke(ProductList $productList): ArrayCollection
    {
        $productList = $this->productRepository->getAll($productList->page(), $productList->limit());
        $productDTOList = new ArrayCollection();
        foreach ($productList->getQuery()->getResult() as $product) {
            $productDTOList->add(ProductDTO::fromProduct($product));
        }

        return $productDTOList;
    }
}
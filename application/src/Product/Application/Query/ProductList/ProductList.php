<?php
declare(strict_types=1);

namespace App\Product\Application\Query\ProductList;

class ProductList
{
    public function __construct(
        private int $page,
        private int $limit
    )
    {
        if ($this->limit > 3) {
            throw new \InvalidArgumentException('You can list only 3 products per page');
        }
    }

    public function page(): int
    {
        return $this->page;
    }

    public function limit(): int
    {
        return $this->limit;
    }
}
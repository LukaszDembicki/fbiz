<?php
declare(strict_types=1);

namespace App\Product\Application\Command\RemoveProduct;

class RemoveProduct
{
    public function __construct(
        private int $productId
    )
    {
    }

    public function productId(): int
    {
        return $this->productId;
    }
}
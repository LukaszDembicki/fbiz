<?php
declare(strict_types=1);

namespace App\Product\Application\Command\RemoveProduct;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\Persistence\ProductRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RemoveProductHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository
    )
    {
    }

    /**
     * @throws \App\Product\Domain\Exception\ProductNotFoundException
     * @throws ProductException
     */
    public function __invoke(RemoveProduct $removeProduct): void
    {
        if ($this->productRepository->countProducts() <= 5) {
            throw new ProductException('You cannot remove another product. There should be at least 5 products in the catalog');
        }

        $this->productRepository->remove($this->productRepository->findOneById($removeProduct->productId()));
    }
}
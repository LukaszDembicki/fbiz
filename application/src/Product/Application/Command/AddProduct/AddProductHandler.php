<?php
declare(strict_types=1);

namespace App\Product\Application\Command\AddProduct;

use App\Product\Domain\Persistence\ProductRepositoryInterface;
use App\Product\Domain\Product;
use App\Product\Domain\ProductPrice;
use App\Product\Domain\ProductTitle;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddProductHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository
    )
    {
    }

    /**
     * @throws \App\Product\Domain\Exception\ProductException
     */
    public function __invoke(AddProduct $addProduct)
    {
        $productDTO = $addProduct->productDTO();
        $product = new Product(
            new ProductTitle($productDTO->title()),
            new ProductPrice($productDTO->price()),
            $productDTO->currency()
        );

        $this->productRepository->save($product);
    }
}
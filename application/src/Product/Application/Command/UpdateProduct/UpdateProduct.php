<?php
declare(strict_types=1);

namespace App\Product\Application\Command\UpdateProduct;

use App\Product\Application\DTO\ProductDTO;

class UpdateProduct
{
    public function __construct(
        private ProductDTO $productDTO
    )
    {
    }

    public function productDTO(): ProductDTO
    {
        return $this->productDTO;
    }
}
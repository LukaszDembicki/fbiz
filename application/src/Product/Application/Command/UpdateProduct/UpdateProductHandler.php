<?php
declare(strict_types=1);

namespace App\Product\Application\Command\UpdateProduct;

use App\Product\Application\Command\AddProduct\AddProduct;
use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\Persistence\ProductRepositoryInterface;
use App\Product\Domain\ProductPrice;
use App\Product\Domain\ProductTitle;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateProductHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepositoryInterface $productRepository
    )
    {
    }

    /**
     * @throws \App\Product\Domain\Exception\ProductException
     * @throws \App\Product\Domain\Exception\ProductNotFoundException
     */
    public function __invoke(UpdateProduct $addProduct)
    {
        $productDTO = $addProduct->productDTO();
        if (empty($productDTO->id())) {
            throw new ProductException('Product ID is required');
        }

        $product = $this->productRepository->findOneById($productDTO->id());
        $product->setTitle(new ProductTitle($productDTO->title()));
        $product->setPrice(new ProductPrice($productDTO->price()));

        $this->productRepository->save($product);
    }
}
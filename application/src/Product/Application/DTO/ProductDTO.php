<?php
declare(strict_types=1);

namespace App\Product\Application\DTO;

use App\Product\Domain\Product;
use JMS\Serializer\Annotation as Serializer;

class ProductDTO
{
    /**
     * @Serializer\Type("integer")
     */
    private ?int $id;

    /**
     * @Serializer\Type("string")
     */
    private ?string $title;

    /**
     * @Serializer\Type("int")
     */
    private ?int $price;

    /**
     * @Serializer\Type("string")
     */
    private ?string $currency;

    public function __construct(?int $id, ?string $title, ?int $price, ?string $currency)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->currency = $currency;
    }

    public static function fromProduct(Product $product): self
    {
        return new self(
            $product->id(),
            $product->title()->title(),
            $product->price()->price(),
            $product->currency()
        );
    }

    public function id(): ?int
    {
        return $this->id;
    }

    public function title(): ?string
    {
        return $this->title;
    }

    public function price(): ?int
    {
        return $this->price;
    }

    public function currency(): ?string
    {
        return $this->currency;
    }
}
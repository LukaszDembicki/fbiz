<?php
declare(strict_types=1);

namespace App\Product\Domain;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Product")
 * @ORM\Entity
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false, name="id", unique=true)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="ProductTitle", nullable=false, name="title")
     */
    private ProductTitle $title;

    /**
     * @ORM\Column(type="ProductPrice", nullable=false, name="price")
     */
    private ProductPrice $price;

    /**
     * @ORM\ManyToMany(targetEntity="App\Quote\Domain\QuoteProduct", mappedBy="quotes")
     */
    private Collection $quoteProduct;

    /**
     * @ORM\Column(type="string", nullable=false, name="currency")
     */
    private string $currency;

    public function __construct(ProductTitle $title, ProductPrice $price, string $currency)
    {
        $this->title = $title;
        $this->price = $price;
        $this->currency = $currency;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function title(): ProductTitle
    {
        return $this->title;
    }

    public function setTitle(ProductTitle $title): void
    {
        $this->title = $title;
    }

    public function price(): ProductPrice
    {
        return $this->price;
    }

    public function setPrice(ProductPrice $price): void
    {
        $this->price = $price;
    }

    public function currency(): string
    {
        return $this->currency;
    }
}
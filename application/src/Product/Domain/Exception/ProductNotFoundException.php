<?php
declare(strict_types=1);

namespace App\Product\Domain\Exception;

class ProductNotFoundException extends \Exception
{

}
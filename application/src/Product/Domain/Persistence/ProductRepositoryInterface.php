<?php
declare(strict_types=1);

namespace App\Product\Domain\Persistence;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\Exception\ProductNotFoundException;
use App\Product\Domain\Product;
use Doctrine\ORM\Tools\Pagination\Paginator;

interface ProductRepositoryInterface
{
    public function getAll(int $page, int $limit): Paginator;

    /**
     * @throws ProductException
     */
    public function countProducts(): int;

    /**
     * @throws ProductNotFoundException
     */
    public function findOneById(int $id): Product;

    public function save(Product $product): void;

    public function remove(Product $product): void;
}
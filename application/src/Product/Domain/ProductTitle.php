<?php
declare(strict_types=1);

namespace App\Product\Domain;

use App\Product\Domain\Exception\ProductException;

class ProductTitle
{
    private const MIN_TITLE_LENGTH = 1;
    private const MAX_TITLE_LENGTH = 80;

    /**
     * @throws ProductException
     */
    public function __construct(
        private string $title
    )
    {
        if (strlen($this->title) <= self::MIN_TITLE_LENGTH) {
            throw new ProductException(sprintf('Blog title has to be greater than %s', self::MIN_TITLE_LENGTH));
        }

        if (strlen($this->title) >= self::MAX_TITLE_LENGTH) {
            throw new ProductException(sprintf('Blog title has to be lesser than %s', self::MAX_TITLE_LENGTH));
        }
    }

    public function title(): string
    {
        return $this->title;
    }
}
<?php
declare(strict_types=1);

namespace App\Product\Domain;

class ProductPrice
{
    public function __construct(
        private int $price
    )
    {
    }

    public function price(): int
    {
        return $this->price;
    }
}
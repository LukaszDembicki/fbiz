<?php
declare(strict_types=1);

namespace App\Product\Infrastructure\Types;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\ProductTitle;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class ProductTitleType extends StringType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->title();
    }

    /**
     * @throws ProductException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new ProductTitle((string)$value);
    }
}
<?php
declare(strict_types=1);

namespace App\Product\Infrastructure\Types;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\ProductPrice;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

class ProductPriceType extends IntegerType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value->price();
    }

    /**
     * @throws ProductException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new ProductPrice((int)$value);
    }
}
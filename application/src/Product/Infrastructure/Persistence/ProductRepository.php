<?php
declare(strict_types=1);

namespace App\Product\Infrastructure\Persistence;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\Exception\ProductNotFoundException;
use App\Product\Domain\Persistence\ProductRepositoryInterface;
use App\Product\Domain\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getAll(int $page, int $limit): Paginator
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from(Product::class, 'p')
            ->setFirstResult($page > 1 ? ($page - 1) * $limit : 0)
            ->setMaxResults($limit);

        return new Paginator($qb);
    }

    /**
     * @throws ProductException
     */
    public function countProducts(): int
    {
        try {
            $qb = $this->getEntityManager()->createQueryBuilder();
            return (int)$qb->select('COUNT(p.id) productCount')
                ->from(Product::class, 'p')
                ->getQuery()
                ->getSingleScalarResult();
        } catch (\Exception $exception) {
            throw new ProductException($exception->getMessage());
        }
    }

    /**
     * @throws ProductNotFoundException
     */
    public function findOneById(int $id): Product
    {
        $product = $this->findOneBy([
            'id' => $id
        ]);

        return $product ?? throw new ProductNotFoundException(sprintf('Product ID %s not found', $id));
    }

    public function save(Product $product): void
    {
        $this->getEntityManager()->persist($product);
        $this->getEntityManager()->flush();
    }

    public function remove(Product $product): void
    {
        $this->getEntityManager()->remove($product);
        $this->getEntityManager()->flush();
    }
}
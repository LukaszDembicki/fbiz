<?php
declare(strict_types=1);

namespace App\Quote\Domain;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Quote")
 * @ORM\Entity
 */
class Quote
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false, name="id", unique=true)
     */
    private int $id;

    /**
     * @ORM\Column(type="string", nullable=false, name="session_id")
     */
    private string $sessionId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Quote\Domain\QuoteProduct", mappedBy="quotes")
     */
    private Collection $quoteProduct;

    public function __construct(int $id, string $sessionId)
    {
        $this->id = $id;
        $this->sessionId = $sessionId;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function sessionId(): string
    {
        return $this->sessionId;
    }
}
<?php
declare(strict_types=1);

namespace App\Quote\Domain;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Quote")
 * @ORM\Entity
 */
class QuoteProduct
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer", nullable=false, name="id", unique=true)
     */
    private int $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Product\Domain\Product", inversedBy="quoteProduct")
     * @ORM\JoinTable(name="Product")
     */
    private Collection $products;

    /**
     * @ORM\ManyToMany(targetEntity="App\Quote\Domain\Quote", inversedBy="quoteProduct")
     * @ORM\JoinTable(name="Quote")
     */
    private Collection $quotes;

    public function __construct(int $id, Collection $products, Collection $quotes)
    {
        $this->id = $id;
        $this->products = $products;
        $this->quotes = $quotes;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function products(): Collection
    {
        return $this->products;
    }

    public function quotes(): Collection
    {
        return $this->quotes;
    }
}
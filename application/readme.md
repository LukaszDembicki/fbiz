<h3>How to run project</h3>
run

```docker-compose up```

then install packages with

```docker exec -i fbiz_php composer install```

project is ready under

```http://localhost:8097```


### How to test
use file `etc/request.http`


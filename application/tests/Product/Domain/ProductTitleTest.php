<?php
declare(strict_types=1);

namespace App\Tests\Product\Domain;

use App\Product\Domain\Exception\ProductException;
use App\Product\Domain\ProductTitle;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 */
class ProductTitleTest extends TestCase
{
    public function testWillThrowExceptionOnInvalidData(): void
    {
        $invalidDataSet = [
            '',
            't',
            'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.'
        ];

        foreach ($invalidDataSet as $title) {
            try {
                new ProductTitle($title);
                $this->fail();
            } catch (ProductException $exception) {
                $this->assertTrue(true);
            }
        }
    }

    /**
     * @throws ProductException
     */
    public function testWillReturnValidDataSet(): void
    {
        $validDataSet = [
            '123456789012345678903'
        ];

        foreach ($validDataSet as $title) {
            $blogPostTitle = new ProductTitle($title);
            $this->assertEquals($title, $blogPostTitle->title());
        }
    }
}

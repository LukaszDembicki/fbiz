CREATE TABLE Product
(
    id       INT AUTO_INCREMENT NOT NULL,
    title    varchar(80) NOT NULL COMMENT '(DC2Type:ProductTitle)',
    price    int         not null COMMENT '(DC2Type:ProductPrice)',
    currency varchar(5)  not null,
    PRIMARY KEY (id)
);

CREATE TABLE Quote
(
    id         INT AUTO_INCREMENT NOT NULL,
    session_id varchar(80) not null,
    created_at DATETIME DEFAULT NULL,
    updated_at DATETIME DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE QuoteProduct
(
    id         INT AUTO_INCREMENT NOT NULL,
    product_id INT NOT NULL,
    quote_id   INT NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE QuoteProduct ADD CONSTRAINT FK_PRODUCT FOREIGN KEY (product_id) REFERENCES Product (id);
ALTER TABLE QuoteProduct ADD CONSTRAINT FK_QUOTE FOREIGN KEY (quote_id) REFERENCES Quote (id) INSERT INTO Product VALUES (1, 'Chocolate', '199', 'USD');

INSERT INTO Product VALUES (2, 'Chips', '299', 'USD');
INSERT INTO Product VALUES (3, 'Beer', '199', 'USD');
INSERT INTO Product VALUES (4, 'Pineapple', '499', 'USD');
INSERT INTO Product VALUES (5, 'Car', '567599', 'USD');